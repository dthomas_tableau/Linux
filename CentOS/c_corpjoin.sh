#!/bin/bash

# Install puppet
sudo rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
sudo yum --disablerepo=* --enablerepo=puppetlabs* clean all
sudo yum -y install puppet

#Download puppet config file from puppet share
echo "Change directory to /etc/puppet & Curl puppet.conf"
cd /etc/puppet/ && curl -O http://puppetshare.dev.tsi.lan/puppet/Linux/PuppetOG/puppet.conf 
chkconfig puppet on

# Enable and start Puppet service
systemctl enable puppet.service #enable
systemctl start puppet          #start

# Enable sudo access for users in AD group development, which will allow sudo access
# after AD stuff is set up. This should probably go in kickstart when we have that going.
sudo bash -c 'echo "%development ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/development'

# Install PBIS repo file
sudo cat > /etc/yum.repos.d/pbiso.repo <<'EOF'
[pbiso]
name=PBISO- local packages for $basearch
baseurl=http://repo.pbis.beyondtrust.com/yum/pbiso/$basearch
enabled=1
gpgkey=http://repo.pbis.beyondtrust.com/yum/RPM-GPG-KEY-pbis
gpgcheck=1
EOF

# Disable all but pbis repo and clean all
sudo yum --disablerepo=* --enablerepo=pbis clean all

# Install latest pbis
sudo yum -y install pbis-open

## TSI.LAN domain join credentials ##
printf "Enter your TSI.LAN username: "
read ADUSER

printf "Enter your TSI.LAN password: "
read -s ADPASS

echo "Joining TSI.LAN"
/opt/pbis/bin/domainjoin-cli join --assumeDefaultDomain yes --userDomainPrefix tsi --ou "TSI Computers/Workstations/Dev Workstations" TSI.LAN "${ADUSER}" "${ADPASS}"

echo "Configuring Default login shell as /bin/bash"
/opt/pbis/bin/config LoginShellTemplate /bin/bash >/dev/null 2>&1

echo "Updating DNS and setting as daily cron job"
echo $'#!/bin/bash\nsudo /opt/pbis/bin/update-dns' > /etc/cron.daily/dns
chmod 755 /etc/cron.daily/dns
/opt/pbis/bin/update-dns

# Add Domain User to Wheel group
sudo gpasswd -a $ADUSER wheel
echo "Added Domain User to Wheel group"

## Install additional Application ##
# Install VIM
sudo yum -y install vim
echo "Installed VIM"

# Install GUI for VIM
sudo yum -y install vim-x11
echo "Installed VIM with GUI"

# Install Screen
sudo yum -y install screen - https://www.rackaid.com/blog/linux-screen-tutorial-and-how-to/
echo "Installed Screen"

# Install TMUX
sudo yum -y install tmux
echo "Installed TMUX"

# Install XRDP
#Install EPEL and nux Desktop repository rpms
rpm -Uvh https://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-9.noarch.rpm #if this fails check website to see if release has changed
rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm

yum -y install xrdp tigervnc-server

systemctl start xrdp.service

netstat -antup | grep xrdp

firewall-cmd --permanent --zone=public --add-port=3389/tcp
firewall-cmd --reload

chcon --type=bin_t /usr/sbin/xrdp
chcon --type=bin_t /usr/sbin/xrdp-sesman

echo "Installed XRDP and Services are running"

# Update to NVIDIA driver

rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org 
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
yum install -y nvidia-detect kmod-nvidia


## REBOOT WHEN COMPLETE
echo "DOMAIN JOIN COMPLETE. You must now reboot your system: 'sudo reboot'"
# Don't actually reboot system in case things went wrong and user wants to debug
#reboot
