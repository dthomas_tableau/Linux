#!/bin/bash

#switch to Root
sudo -i

read -p "CentOS (y)?" OS
if [ "$OS" = "y" ]; then
  #yes will install Redhat core tools for CentOS
  yum install -y redhat-lsb-core
else
  #yes will install Redhat core tools for Ubuntu
  add-apt-repository "deb http://cz.archive.ubuntu.com/ubuntu trusty main"
  apt-get update
  apt-get install -y lsb-core
fi

#Set variable for OS
swver=$(lsb_release -d | awk '{print $2}')

#Install git
if [ $swver = "CentOS" ]; then
yum install -y git
fi

if [ $swver = "Ubuntu" ]; then
apt-get install -y git
fi

#download gitlab repostitory
git clone https://gitlab.tableausoftware.com/tools/linux-scripts.git

#change directory to Linux-Scirpts folder
cd linux-scrips

#load gitlab branch CentOS_update no will just continue wo loading branch
read -p "Load GitLab Branch (y/n)?" Branch
if [ "$Branch" = "y" ]; then
git checkout CentOS_update
fi

#Make everything folder an executable
chmod a+x *


#From Variable run O/S setup Script
if [ $swver = "CentOS" ]; then
source $(dirname $0)/CentOS
fi

if [ $swver = "Ubuntu" ]; then
source $(dirname $0)/u_corpjoin.sh
fi


